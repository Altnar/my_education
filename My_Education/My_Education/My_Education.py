import csv
import urllib.request
from bs4 import BeautifulSoup


CSU_NOW_URL = 'http://www.csu.ru/roles/prospective-students/2017/bakplan2.aspx'


def get_informatin():
    soup = BeautifulSoup(get_html(CSU_NOW_URL))
    chelgu_new = soup.find_all('tr')[3:]
    faculty = ''
    spec = ''
    places = 0
    csu_list = []
    for each in chelgu_new:
        if str(each).find('Филиал') > -1: break
        if str(each).find('факультет') > -1 or str(each).find('Факультет') > -1 or str(each).find('Институт') > -1:
            faculty = each.text
            faculty = str(faculty)
            temp = 0
            for letter in str(faculty):
                if letter.isdigit(): faculty = str(faculty)[:temp]
                temp = temp + 1
            spec = (each.find_all('td')[2].text)
            places = (each.find_all('td')[4].text)
        else:
            spec = (each.find_all('td')[1].text)
            places = (each.find_all('td')[3].text)
        csu_list.append({'Faculty': str(faculty),
                         'Spec': str(spec),
                         'Places': str(places)})
    for row in csu_list:
        print('Факультет: ' + row['Faculty'])
        print('Специальность: ' + row['Spec'])
        print('Места: ' + row['Places'])
        print()




def get_html(url):
    response = urllib.request.urlopen(url)
    return response.read()



def main():
  get_informatin()

if __name__ == '__main__':
    main()
